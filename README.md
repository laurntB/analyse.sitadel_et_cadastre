---
title: analyse.sitadel_et_cadastre
date: 03/02/2024
---


# Objectif : rendre géographiques les informations des tableaux sitadel

Ce projet `R` en incubation	fournit une fonction `build_gpkg` qui
prend en entrée une liste structurée organisant les données
surlesquelles va opérer le traitement associées à quelques options. 

En sortie, la fonction renvoi un objet composé utilisable en R pour
poursuivre l'analyse des données sitadel, mais surtout, écrit
parallèlement sur le disque un fichier qpkg qui est exploitable avec
QGis.

Les données traitées sont issues du système d'information sitadel pour
sa partie librement diffusée en OpenData.


# Installation

Liste des dépendances à installer préalablement:

  - tidyverse
  - [sitadel.opendata](https://gitlab.com/laurntB/sitadel.opendata.git)


# Usage par l'exemple

    devtools::document("../sitadel.opendata") # dépendance requise
    source("./build_gpkg_lib.R")
    
    
    source("./sources-donnees.R")       ## catalogue des données par confort
	                                    ## ce catalogue ne fonction que dans l'environnement Deal972 
    datasrc <- datasrc_MQ_multicad.myPC ## à choisir entre : ls(pattern="^datasrc_")
    
    results <- build_gpkg(datasrc)      ## mais surtout génère un fichier geopackage
                                        ## utilisable par QGis
  
# Paramètre d'entrée pour l'utilisation de `build_gpkg`

Le travail de constitution de ce paramètre d'entrée est essentiel,
c'est la plus lourde part du travail que l'utilisateur aura à fournir
s'il veut utiliser la fonction `build_gpkg` que ce projet vise à diffuser.

L'entrée à fournir à la fonction build package est d'autemment moins
trivial qu'on souhaitera faire le plus de lien possible des dossiers
sitadel avec des parcelles cadastrale connue.

Pour maximiser la *jointure*, il sera donc essentiel de faire balayer
au traitement le plus grand nombre de millesimes de cadastre possible.

La structuration des données correspond à une liste/dictionnaire du
genre suivant :

    datasrc_MQlbn <- list(
      cibles = list(
        REG_in = '02',
        REG_mark = 'MQ'
      ),
      donnees_sitadel_opendata = list(
        millesime = "2023-09",         # "2023-06"
        dicos_version_in = "20230928", # "20220501"
        dir = "C:\\Users\\laurent.beltran\\Documents\\_sandbox_\\__inputs__\\sdes\\sitadel2\\opendata"
      ),
      donnees_cadastrales_crs_ref = 5490,
      donnees_cadastrales = list(
        '2022' = list(
          fpth2_cadastre = './data/CAD-Cadastre_2022/Cadastre2022_5490_cat_prop.shp',
          ftdau_geo_parcel =  NULL,
          ftcadastre_geo_parcel =  NULL 
        )
      ),
      output = list(
        model_name="./data/sitadel_parcelles-{REG_mark}.gpkg"
      )
    )

À ce stade la finalité de chaque *sockets* devrait intuitivement se
comprendre par sa propre dénommination.

Il est simplement à confirmer que le socket `donnees_cadastrales`
correspond à une liste qui peut accepter plusieurs millesime.

# Statut

Ce projet a été utilisé par unité statistique de la Martinique et de
la Guyane pour produire leur propre analyse et a donc répondu à leur
besoin.

Cependant les données cadastrales étant très lourdes, nous ne savons
pas comment se comporterait le script avec des cadastres beaucoup plus
volumineux en entrée pour des départements de france hexagonale par
exemple.

Nous n'avons pas seulement tenter de le prédire, dans un premier temps
il suffisat que cela fonctionne pour nous. Nous restons cependant
ouvert à collaborer avec tous les collègues qui seraient intéressaient
à nous rejoindre dans cette avanture.

# Futur des idées et de ce qu'il reste à faire

Dans l'immédiat,

  - documenter les fonctions, les paramètres
  - transformer le tout en package
  - produire une documentation plus rigoureuse de la constitution du
    paramètre d'entrée, voire séparer en trois paramètre la signature
    de la fonction (sitadel + cadastre + output).
	
	



